<?php


namespace Ipex\Censor;


/**
 * Class CensorWordsResult
 * @package Snipe\BanBuilder
 *
 * @property  string $orig Origin string;
 * @property  string $clean Cleaned string;
 * @property  array $matched Found swear words;
 *
 */
class CensorWordsResult
{
    protected $result;

    protected $accessable = ['orig', 'clean', 'matched'];

    public function __get($name)
    {
        if (in_array($name, $this->accessable))
            return $this->result[$name];
        throw new \Exception('Undefined property: ' . static::class . "::" . $name);
    }

    public function __construct(array $result)
    {
        $this->result = $result;
    }

    public function isSwearWord(): bool
    {
        return count($this->matched) != 0;
    }

}
